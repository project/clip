<?php

namespace Drupal\clip\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the Clip entity.
 *
 * @ingroup clip
 *
 * @ContentEntityType(
 *   id = "clip",
 *   label = @Translation("clip"),
 *   base_table = "clip",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "field_media" = "field_media",
 *     "field_title" = "field_title",
 *     "field_in" = "field_in",
 *     "field_out" = "field_out",
 *   },
 * )
 */
class Clip extends ContentEntityBase implements ContentEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Clip entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Clip entity.'))
      ->setReadOnly(TRUE);
    $fields['field_media'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Media'))
      ->setCardinality(1)
      ->setDescription(t('The playable Media entity this Clip is based on.'))
      ->setSetting('target_type', 'media');
    $fields['field_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The Clip title.'))
      ->setDefaultValue('');
    $fields['field_in'] = BaseFieldDefinition::create('string')
      ->setLabel(t('In Point'))
      ->setDescription(t('The "timecode" (HH:MM:SS:FF) representing the start point for the Clip.'))
      ->setSetting('max_length', 11)
      ->setDefaultValue('');
    $fields['field_out'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Out Point'))
      ->setDescription(t('The "timecode" (HH:MM:SS:FF) representing the end point for the Clip.'))
      ->setSetting('max_length', 11)
      ->setDefaultValue('');
    return $fields;
  }

}
