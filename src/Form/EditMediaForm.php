<?php

namespace Drupal\clip\Form;

use Drupal\clip\Entity\Clip;
use Drupal\clip\UrlWrapperInterface;
use Drupal\clip\Service\ClipFactoryInterface;
use Drupal\clip\Service\UrlWrapperFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormStateInterface;
use Drupal\formfactory\Services\FormFactoryInterface;
use Drupal\formfactorykits\Kits\FormFactoryKitInterface;
use Drupal\formfactorykits\Services\FormFactoryKitsInterface;
use Drupal\media\MediaForm as BaseMediaForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Container Injection Interface.
 */
class EditMediaForm extends BaseMediaForm implements ContainerInjectionInterface {

  /**
   * Module Extension List.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $modules;

  /**
   * Form Factory Interface.
   *
   * @var \Drupal\formfactory\Services\FormFactoryInterface
   */
  protected FormFactoryInterface $formFactory;

  /**
   * Form Factory Kits Interface.
   *
   * @var \Drupal\formfactorykits\Services\FormFactoryKitsInterface
   */
  protected FormFactoryKitsInterface $kits;

  /**
   * Clip Factory Interface.
   *
   * @var \Drupal\clip\Entity\ClipFactoryInterface
   */
  protected ClipFactoryInterface $clipFactory;

  /**
   * Url Wrapper Factory Interface.
   *
   * @var \Drupal\clip\Service\UrlWrapperFactoryInterface
   */
  protected UrlWrapperFactoryInterface $urlWrapperFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    ModuleExtensionList $moduleExtensionList,
    FormFactoryInterface $formFactory,
    FormFactoryKitsInterface $kits,
    ClipFactoryInterface $clipFactory,
    UrlWrapperFactoryInterface $urlWrapperFactory
  ) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
    $this->modules = $moduleExtensionList;
    $this->formFactory = $formFactory;
    $this->kits = $kits;
    $this->clipFactory = $clipFactory;
    $this->urlWrapperFactory = $urlWrapperFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('extension.list.module'),
      $container->get('formfactory'),
      $container->get('formfactorykits'),
      $container->get('clip.factory'),
      $container->get('clip.url_wrapper'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Only modify the form if the Provider Entity is supported.
    $url = $form['field_media_oembed_video']['widget'][0]['value']['#default_value'];
    if (empty($url)) {
      return $form;
    }
    $urlWrapper = $this->urlWrapperFactory->getUrlWrapper($url);
    if (!$urlWrapper) {
      return $form;
    }

    $this->checkTriggeringElement($form_state);

    // Append the Clip Edit form elements to the form.
    $this->formFactory->load($form);
    $this->formFactory->attach('clip/media-entity-edit-form')
      ->setDrupalSetting('clipMediaEntityEditForm', 'isInOutPlaybackMode', FALSE)
      ->setDrupalSetting('clipMediaEntityEditForm', 'isLoopedPlaybackMode', FALSE)
      ->setDrupalSetting('clipMediaEntityEditForm', 'defaultVolumeLevel', .2)
      ->append($this->kits->container('clip_editor')
        ->append($this->getPlayerElement($urlWrapper))
        ->append($this->getTimestampsContainer())
        ->append($this->getControlButtonsContainer())
        ->append($this->getClipDetailsContainer())
        ->append($this->getCrudButtonsContainer())
        ->append($this->getClipsContainer()));

    // Return the modified form.
    return $this->formFactory->getForm();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function checkTriggeringElement(FormStateInterface $form_state) {
    $e = $form_state->getTriggeringElement();
    if (!$e) {
      return;
    }
    switch ($e['#name']) {
      case 'create_clip':
        $in = $form_state->getValue('clip_in_ts');
        $out = $form_state->getValue('clip_out_ts');
        if ($this->getClipByInOut($in, $out)) {
          $form_state->setError($e, 'Clip with identical in/out points already exists.');
        }
        else {
          $clip = Clip::create();
          $clip->set('field_media', $this->getMediaId());
          $clip->set('field_title', $form_state->getValue('clip_title'));
          $clip->set('field_in', $in);
          $clip->set('field_out', $out);
          $clip->save();
          $form_state->setRebuild();
        }
        break;

      case 'update_clip':
        $id = $form_state->getValue('clip');
        if ($id) {
          $clip = Clip::load($id);
          $clip->set('field_title', $form_state->getValue('clip_title'));
          $clip->set('field_in', $form_state->getValue('clip_in_ts'));
          $clip->set('field_out', $form_state->getValue('clip_out_ts'));
          $clip->save();
          $form_state->setRebuild();
        }
        break;

      case 'delete_clip':
        $id = $form_state->getValue('clip');
        if ($id) {
          Clip::load($id)->delete();
          $form_state->setRebuild();
        }
        break;

      default:
        throw new \DomainException('Unsupported triggering element:' . $e['#name']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxFormRebuild(array &$form, FormStateInterface $form_state) {
    return $form['clip_editor']['clips'];
  }

  /**
   * {@inheritdoc}
   */
  public function getControlButtonsContainer(): FormFactoryKitInterface {
    // @todo use industry-standard icons.
    // @todo improve accessibility for buttons in this container.
    // @todo add helper text that appears on mouse-over.
    return $this->kits->container('control_buttons')
      ->append($this->kits->button('goto_in')->setValue('>'))
      ->append($this->kits->button('set_in')->setValue('{'))
      ->append($this->kits->button('previous')->setValue('--'))
      ->append($this->kits->button('play')->setValue('▶'))
      ->append($this->kits->button('play_in_out')->setValue('|▶|'))
      ->append($this->kits->button('next')->setValue('++'))
      ->append($this->kits->button('set_out')->setValue('}'))
      ->append($this->kits->button('goto_out')->setValue('<'));
  }

  /**
   * {@inheritdoc}
   */
  public function getClipsContainer(): FormFactoryKitInterface {
    return $this->kits->container('clips')
      ->wrap()
      ->append($this->getClipsTable());
  }

  /**
   * {@inheritdoc}
   */
  public function getClipsTable($currentClipId = NULL): FormFactoryKitInterface {
    $clips = $this->kits->tableSelect('clip')
      // @todo >setSticky()
      ->setSingle()
      ->setEmptyMessage($this->t('No related Clips were found.'))
      ->appendHeaderColumn('name', $this->t('Name'))
      ->appendHeaderColumn('in', $this->t('In Point'))
      ->appendHeaderColumn('out', $this->t('Out Point'));
    $clipIds = $this->getClipIds();
    if ($currentClipId) {
      $clips->setDefaultValue($currentClipId);
    }
    else {
      $clips->setDefaultValue(current($clipIds));
    }
    foreach ($clipIds as $clipId) {
      $clip = Clip::load($clipId);
      $clips->appendOption($clipId, [
        'name' => $clip->get('field_title')->getString(),
        'in' => $clip->get('field_in')->getString(),
        'out' => $clip->get('field_out')->getString(),
      ]);
    }
    return $clips;
  }

  /**
   * {@inheritdoc}
   */
  public function getClipDetailsContainer(): FormFactoryKitInterface {
    return $this->kits->container('clip_details')
      ->append($this->kits->textField('clip_title')->setTitle($this->t('Title')));
  }

  /**
   * {@inheritdoc}
   */
  public function getTimestampsContainer(): FormFactoryKitInterface {
    return $this->kits->container('timestamps')
      ->append($this->kits->textField('clip_in_ts'))
      ->append($this->kits->textField('clip_current_ts'))
      ->append($this->kits->textField('clip_out_ts'));
  }

  /**
   * {@inheritdoc}
   */
  public function getCrudButtonsContainer(): FormFactoryKitInterface {
    return $this->kits->container('crud_buttons')
      ->append($this->kits->submit('delete_clip')
        ->setValue($this->t('Delete Current Clip'))
          // @todo show JS confirmation prompt
        ->setAjax('clips-wrapper'))
      ->append($this->kits->submit('update_clip')
        ->setValue($this->t('Update Current Clip'))
          // @todo show JS confirmation prompt 'Are you sure you want to update "@clip"?'
        ->setAjax('clips-wrapper'))
      ->append($this->kits->submit('create_clip')
        ->setValue($this->t('Create New Clip'))
        ->setAjax('clips-wrapper'));
  }

  /**
   * {@inheritdoc}
   */
  public function getPlayerElement(UrlWrapperInterface $urlWrapper): FormFactoryKitInterface {
    $player = $this->getPlayerName($urlWrapper);
    return $this->kits->template('player')
      ->setTemplateFile(vsprintf('%s/templates/players/%s.twig', [
        $this->modules->getPath('clip'),
        $player,
      ]))
      ->attach('clip/player')
      ->attach('clip/player-' . $player);
  }

  /**
   * {@inheritdoc}
   */
  private function getMediaId(): int {
    return $this->getRouteMatch()->getRawParameter('media');
  }

  /**
   * {@inheritdoc}
   */
  private function getClipIds(): array {
    return $this->clipFactory->getClipIds($this->getMediaId());
  }

  /**
   * {@inheritdoc}
   */
  private function getClipByInOut(string $in, string $out): ?Clip {
    $result = $this->entityTypeManager->getStorage('clip')
      ->getQuery()
      ->accessCheck()
      ->condition('field_in', $in)
      ->condition('field_out', $out)
      ->execute();
    return $result ? Clip::load(current($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function getPlayerName(UrlWrapperInterface $urlWrapper): string {
    $userPlayerPreferences = $this->getUserPlayerPreferences($urlWrapper->getCompanyMachineName());
    foreach ($userPlayerPreferences as $userPlayerPreference) {
      if ($urlWrapper->isSupportedPlayer($userPlayerPreference)) {
        return $userPlayerPreference;
      }
    }
    return 'unsupported';
  }

  /**
   * {@inheritdoc}
   */
  private function getUserPlayerPreferences(string $provider): array {
    // @todo load user player preferences from database.
    $userPreferences = [
      'youtube' => ['videojs', 'youtube'],
      'vimeo' => ['videojs', 'vimeo'],
    ];
    return $userPreferences[$provider] ?? [];
  }

}
