(function ($, Drupal) {
  let player;
  let currentField, inField, titleField, outField;
  let inBtn, gotoInBtn, outBtn, gotoOutBtn, playBtn, playInOutBtn, prevBtn, nextBtn;

  Drupal.behaviors.clipMediaEntityEditForm = {
    attach: function (context, settings) {
      player = new ClipPlayer();

      inField = inField || onceFirst('in-field', '[id=edit-clip-in-ts]', context);
      currentField = currentField || onceFirst('current-field', '[id=edit-clip-current-ts]', context);
      outField = outField || onceFirst('out-field', '[id=edit-clip-out-ts]', context);
      titleField = titleField || onceFirst('title-field', '[id=edit-clip-title]', context);

      inBtn = inBtn || onceFirst('set-in', '[id=edit-set-in]', context);
      inBtn.addEventListener('click', function (event) {
        event.preventDefault();
        markInRequest();
      }, FALSE);

      gotoInBtn = gotoInBtn || onceFirst('goto-in', '[id=edit-goto-in]', context);
      gotoInBtn.addEventListener('click', function (event) {
        event.preventDefault();
        goToInRequest();
        pauseRequest();
      }, FALSE);

      outBtn = outBtn || onceFirst('set-out', '[id=edit-set-out]', context);
      outBtn.addEventListener('click', function (event) {
        event.preventDefault();
        markOutRequest();
      }, FALSE);

      gotoOutBtn = gotoOutBtn || onceFirst('goto-out', '[id=edit-goto-out]', context);
      gotoOutBtn.addEventListener('click', function (event) {
        event.preventDefault();
        goToOutRequest();
        pauseRequest();
      }, FALSE);

      playBtn = playBtn || onceFirst('play', '[id=edit-play]', context);
      playBtn.addEventListener('click', function (event) {
        event.preventDefault();
        player.toggle();
      }, FALSE);

      playInOutBtn = playInOutBtn || onceFirst('play-in-out', '[id=edit-play-in-out]', context);
      playInOutBtn.addEventListener('click', function (event) {
        event.preventDefault();
        playInOutRequest();
      }, FALSE);

      prevBtn = prevBtn || onceFirst('previous', '[id=edit-previous]', context);
      prevBtn.addEventListener('click', function (event) {
        event.preventDefault();
        if (player.isPlaying()) {
          player.pause();
        }
        player.seekToSecondRelative(-1);
      }, FALSE);

      nextBtn = nextBtn || onceFirst('next', '[id=edit-next]', context);
      nextBtn.addEventListener('click', function (event) {
        event.preventDefault();
        if (player.isPlaying()) {
          player.pause();
        }
        player.seekToSecondRelative(1);
      }, FALSE);

      let elements = once('clips-radio-buttons', '[id=clips-wrapper] input', context);
      if (elements.length) {
        clipRds = elements;
      }
      if (typeof clipRds !== 'undefined') {
        for (let i = 0; i < clipRds.length; i++) {
          clipRds[i].addEventListener('click', function (event) {
            loadClipFromRadioElement(event.target);
          }, FALSE);
        }
      }

      // Support hotkeys when focused on the play button.
      // @todo use common editing hotkeys
      playBtn.addEventListener('keypress', logKey);
      function logKey(e) {
        e.preventDefault();
        switch(e.code) {
          case 'BracketLeft':
            player.volumeDown();
            break;

          case 'BracketRight':
            player.volumeUp();
            break;

          case 'Comma':
            goToOutRequest(inField, outField);
            break;

          case 'Period':
            goToInRequest(inField, outField);
            break;

          case 'KeyI':
          case 'KeyZ':
            markInRequest(inField, outField);
            break;

          case 'Minus':
          case 'KeyQ':
            previousFrameRequest(inField, outField);
            break;

          case 'Equal':
          case 'KeyE':
            nextFrameRequest(inField, outField);
            break;

          case 'KeyO':
          case 'KeyC':
            markOutRequest(inField, outField);
            break;

          case 'KeyA':
          case 'KeyJ':
            shuttleLeftRequest();
            break;

          case 'KeyS':
            normalSpeedRequest();
            break;

          case 'KeyK':
          case 'KeyW':
            shuttleStopRequest();
            break;

          case 'KeyD':
          case 'KeyL':
            shuttleRightRequest();
            break;

          case 'KeyF':
            plusSecondsRequest(60);
            break;

          case 'KeyG':
            plusSecondsRequest(90);
            break;

          case 'KeyH':
            plusSecondsRequest(180);
            break;

          case 'KeyX':
            normalSpeedRequest();
          case 'Enter':
            playInOutRequest();
            break;
        }
      }
    },

    onPlayerAttach: function () {
      player.setVolume(drupalSettings.clipMediaEntityEditForm.defaultVolumeLevel);
      let clipRadioElement = getCurrentClip();
      if (typeof clipRadioElement !== 'undefined') {
        loadClipFromRadioElement(clipRadioElement);
      }
      setInterval(function () {
        if (!player.isPlaying()) {
          return;
        }
        let currentTime = player.getCurrentTime();
        if (drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode) {
          if (currentTime >= player.getTime(outField.value)) {
            if (drupalSettings.clipMediaEntityEditForm.isLoopedPlaybackMode) {
              goToIn();
            }
            else {
              pauseRequest();
            }
          }
        }
        currentField.setAttribute('value', player.getTimecode(currentTime));
      }, 1000 / player.getFrameRate());
    },

    onPlayerPause: function () {
      playBtn.setAttribute('value', '\u25B6');
    },

    onPlayerPlay: function () {
      playBtn.setAttribute('value', '\u23F8');
    },

    getVideoSourceUrl() {
      return new URL(document.getElementById('edit-field-media-oembed-video-0-value').value);
    },
  };

  const onceFirst = function (id, query, context) {
    let elements = once(id, query, context);
    if (elements.length) {
      return elements[0];
    }
  }

  const getCurrentClip = function () {
    if (typeof clipRds === 'undefined') {
      return;
    }
    for(let i = 0; i < clipRds.length; i++) {
      if (clipRds[i].checked) {
        return clipRds[i];
      }
    }
  };

  const loadClipFromRadioElement = function (clipRadioElement) {
    let tds = clipRadioElement.parentElement.parentElement.parentElement.getElementsByTagName('td');
    titleField.value = tds[1].innerHTML;
    inField.value = tds[2].innerHTML;
    outField.value = tds[3].innerHTML;
    goToIn();
  };

  const playInOutRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = TRUE;
    goToIn();
    if (!player.isPlaying()) {
      player.play();
    }
  };

  const pauseRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.pause();
  };

  const previousFrameRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    if (player.isPlaying()) {
      player.pause();
    }
    player.seekToFrameRelative(-1);
  };

  const nextFrameRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    if (player.isPlaying()) {
      player.pause();
    }
    player.seekToFrameRelative(1);
  };

  const shuttleLeftRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.shuttleLeft();
  };

  const normalSpeedRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.setPlaybackRate(1);
    player.play();
  };

  const shuttleStopRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.pause();
  };

  const shuttleSlowRightRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.setPlaybackRate(.25);
    player.play();
  };

  const shuttleRightRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.shuttleRight();
  };

  const minusSecondsRequest = function (seconds) {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.seekToSecondRelative(seconds);
  };

  const plusSecondsRequest = function (seconds) {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.seekToSecond(player.getCurrentTime() + seconds);
  };

  const markInRequest = function () {
    let timecode = player.getTimecode(player.getCurrentTime());
    let seconds = player.getTime(timecode);
    inField.value = timecode;
    if (outField.value && seconds > player.getTime(outField.value)) {
      outField.value = player.getTimecode(seconds + 10);
    }
  };

  const goToIn = function () {
    player.seekToTimecode(inField.value);
  };

  const goToInRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.seekToTimecode(inField.value);
    player.pause();
  };

  const markOutRequest = function () {
    let timecode = player.getTimecode(player.getCurrentTime());
    outField.value = timecode;
    if (player.getTime(inField.value) > player.getTime(outField.value)) {
      inField.value = timecode;
    }
  };

  const goToOutRequest = function () {
    drupalSettings.clipMediaEntityEditForm.isInOutPlaybackMode = FALSE;
    player.seekToTimecode(outField.value);
    player.pause();
  };

})(jQuery, Drupal);
