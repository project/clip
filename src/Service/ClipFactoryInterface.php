<?php

namespace Drupal\clip\Service;

use Drupal\clip\Entity\Clip;

/**
 * Implements Interface.
 */
interface ClipFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function create(array $values): Clip;

  /**
   * {@inheritdoc}
   */
  public function getClipIds(int $mid): array;

}
