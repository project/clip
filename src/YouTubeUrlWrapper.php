<?php

namespace Drupal\clip;

/**
 * Implements Youtube Url Wrapper.
 */
class YouTubeUrlWrapper extends UrlWrapper {

  /**
   * {@inheritdoc}
   */
  public function getCompanyMachineName(): string {
    return 'youtube';
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    $queryString = parse_url($this->url, PHP_URL_QUERY);
    parse_str($queryString, $parts);
    if (empty($parts['v'])) {
      throw new \UnexpectedValueException('Could not get entity id.');
    }
    return $parts['v'];
  }

  /**
   * {@inheritdoc}
   */
  public function getIdQueryString(): string {
    return 'v=' . $this->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedPlayers(): array {
    return [
      'videojs',
      'youtube',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isSupportedPlayer(string $player): bool {
    return in_array($player, $this->getSupportedPlayers());
  }

}
