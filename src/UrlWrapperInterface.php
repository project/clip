<?php

namespace Drupal\clip;

/**
 * Implements Interface.
 */
interface UrlWrapperInterface {

  /**
   * {@inheritdoc}
   */
  public function getId(): string;

  /**
   * {@inheritdoc}
   */
  public function getIdQueryString(): string;

  /**
   * {@inheritdoc}
   */
  public function getCompanyMachineName(): string;

  /**
   * {@inheritdoc}
   */
  public function getProviderDomain(): string;

  /**
   * {@inheritdoc}
   */
  public function getSupportedPlayers(): array;

  /**
   * {@inheritdoc}
   */
  public function isSupportedPlayer(string $player): bool;

}
