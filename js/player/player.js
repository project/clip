class AbstractClipPlayer {
  pause() {
    throw new Error('`pause` not implemented');
  }

  play() {
    throw new Error('`play` not implemented');
  }

  toggle() {
    if (this.isPlaying()) {
      this.pause();
      this.setPlaybackRate(1);
    }
    else {
      this.play();
    }
  }

  setPlaybackRate(rate) {
    throw new Error('`setPlaybackRate` not implemented');
  }

  getCurrentTime() {
    throw new Error('`getCurrentTime` not implemented');
  }

  seekToFrameRelative(amount) {
    let frameLength = this.getFramesLength(1);
    let diff = (amount > 0) ? frameLength : -frameLength;
    this.seekToSecond(this.getCurrentTime() + diff);
  }

  seekToSecondRelative(amount) {
    this.seekToSecond(this.getCurrentTime() + amount);
  }

  seekToSecond(second) {
    throw new Error('`seekToSecond` not implemented');
  }

  seekToTimecode(timecode) {
    throw new Error('`seekToTimecode` not implemented');
  }

  shuttleLeft() {
    this.seekToSecondRelative(-10);
  }

  shuttleRight() {
    this.setPlaybackRate(2);
  }

  isPlaying() {
    throw new Error('`isPlaying` not implemented');
  }

  getVolume() {
    throw new Error('`getVolume` not implemented');
  }

  setVolume(volume) {
    throw new Error('`setVolume` not implemented');
  }

  volumeUp() {
    let volume = this.getVolume();
    volume += .1;
    if (volume > 1) {
      volume = 1;
    }
    this.setVolume(volume);
  }

  volumeDown() {
    let volume = this.getVolume();
    volume -= .1;
    if (volume < 0) {
      volume = 0;
    }
    this.setVolume(volume);
  }

  /**
   * @see https://stackoverflow.com/a/31385814
   */
  getTime(timecode) {
    if (!timecode) {
      return '00:00:00:00';
    }
    let timeArray = timecode.split(":");
    let hours     = parseInt(timeArray[0]) * 60 * 60;
    let minutes   = parseInt(timeArray[1]) * 60;
    let seconds   = parseInt(timeArray[2]);
    let frames    = parseInt(timeArray[3]) * (1 / this.getFrameRate());
    return hours + minutes + seconds + frames;
  }

  /**
   * @see https://stackoverflow.com/a/6313032
   */
  getTimecode(time) {
    let date = new Date(time * 1000);
    let h = date.getUTCHours();
    let m = date.getUTCMinutes();
    let s = date.getSeconds();
    let f = Math.round((date.getMilliseconds() / 1000) * this.getFrameRate());
    if (h > 12) {
      h = h % 12;
    }
    if (h < 10) { h = "0" + h; }
    if (m < 10) { m = "0" + m; }
    if (s < 10) { s = "0" + s; }
    if (f < 10) { f = "0" + f; }
    return h + ':' + m + ':' + s + ':' + f;
  }

  getFrameRate() {
    // @todo get current video frame rate
    return 30;
  }

  getFramesLength(frames) {
    let oneFrameLength = 1 / this.getFrameRate();
    return oneFrameLength * frames;
  }
}
