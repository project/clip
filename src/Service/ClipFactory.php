<?php

namespace Drupal\clip\Service;

use Drupal\clip\Entity\Clip;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Component\Uuid\Php as UuidService;

/**
 * Implements Clip Factory.
 */
class ClipFactory implements ClipFactoryInterface {

  /**
   * Entity Storage Interface.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $storage;

  /**
   * UUID Service.
   *
   * @var \Drupal\Component\Uuid\Php
   */
  private UuidService $uuid;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager, UuidService $uuid) {
    $this->uuid = $uuid;
    $this->storage = $entityTypeManager->getStorage('clip');
  }

  /**
   * {@inheritdoc}
   */
  public function create(array $values): Clip {
    return Clip::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public function getClipIds(int $mid): array {
    return $this->storage->getQuery()
      ->condition('field_media', $mid)
      ->accessCheck()
      ->sort('field_in')
      ->execute();
  }

}
