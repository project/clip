let player;

class ClipPlayer extends AbstractClipPlayer {
  pause() {
    player.pause();
  }

  play() {
    player.play();
  }

  isPlaying() {
    return player.paused() === FALSE;
  }

  setPlaybackRate(rate) {
    player.playbackRate(rate);
  }

  seekToSecond(second) {
    player.currentTime(second);
  }

  seekToTimecode(timecode) {
    player.currentTime(this.getTime(timecode));
  }

  getVolume() {
    return player.volume();
  }

  setVolume(volume) {
    player.volume(volume);
  }

  getCurrentTime() {
    return player.currentTime();
  }
}

(function ($, Drupal) {
  Drupal.behaviors.clipVideoJsPlayer = {
    attach: function (context, settings) {
      if (context === document) {
        player = videojs('player', {
          autoplay: TRUE,
          preload: "auto",
          playbackRates: [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2],
          responsive: FALSE,
          sources: [
            {
              type: "video/youtube",
              src: Drupal.behaviors.clipMediaEntityEditForm.getVideoSourceUrl().href,
            },
          ],
        }, Drupal.behaviors.clipMediaEntityEditForm.onPlayerAttach);

        player.on('playing', function () {
          Drupal.behaviors.clipMediaEntityEditForm.onPlayerPlay();
        });

        player.on('pause', function () {
          Drupal.behaviors.clipMediaEntityEditForm.onPlayerPause();
        });
      }
    }
  }
})(jQuery, Drupal);
