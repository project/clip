<?php

namespace Drupal\clip\Form;

use Drupal\clip\Entity\Clip;
use Drupal\clip\Service\ClipFactoryInterface;
use Drupal\clip\Service\UrlWrapperFactoryInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\formfactory\Services\FormFactoryInterface;
use Drupal\formfactorykits\Services\FormFactoryKitsInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Container Injection Interface.
 */
class AddRemoteVideoEntitiesForm extends FormBase implements ContainerInjectionInterface {

  /**
   * Module Extension List.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $modules;

  /**
   * Form Factory Interface.
   *
   * @var \Drupal\formfactory\Services\FormFactoryInterface
   */
  protected FormFactoryInterface $formFactory;

  /**
   * Form Factory Kits Interface.
   *
   * @var \Drupal\formfactorykits\Services\FormFactoryKitsInterface
   */
  protected FormFactoryKitsInterface $kits;

  /**
   * Clip Factory Interface.
   *
   * @var \Drupal\clip\Entity\ClipFactoryInterface
   */
  protected ClipFactoryInterface $clipFactory;

  /**
   * Url Wrapper Factory Interface.
   *
   * @var \Drupal\clip\Service\UrlWrapperFactoryInterface
   */
  protected UrlWrapperFactoryInterface $urlWrapperFactory;

  /**
   * Yaml.
   *
   * @var \Drupal\Component\Serialization\Yaml
   */
  private Yaml $yaml;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    ModuleExtensionList $moduleExtensionList,
    FormFactoryInterface $formFactory,
    FormFactoryKitsInterface $kits,
    ClipFactoryInterface $clipFactory,
    UrlWrapperFactoryInterface $urlWrapperFactory,
    Yaml $yaml,
    MessengerInterface $messenger,
  ) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
    $this->modules = $moduleExtensionList;
    $this->formFactory = $formFactory;
    $this->kits = $kits;
    $this->clipFactory = $clipFactory;
    $this->urlWrapperFactory = $urlWrapperFactory;
    $this->yaml = $yaml;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('extension.list.module'),
      $container->get('formfactory'),
      $container->get('formfactorykits'),
      $container->get('clip.factory'),
      $container->get('clip.url_wrapper'),
      $container->get('serialization.yaml'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clip_import_remote_videos';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->formFactory->load($form);
    $this->formFactory->append(
        $this->kits->textArea('data')
          ->setRequired()
          ->setTitle($this->t('Data'))
          ->setDescription($this->t('A YAML list of remote videos to create entities for. All created entities are unpublished unless stated otherwise.'))
      )
      ->append($this->kits->details('example')
        ->setTitle($this->t('Example'))
        ->append(
          $this->kits->example()
            ->setExample($this->getExample())
            ->setSuffix($this->t('Copy and paste all or part of the example above into the text area & submit the form to get started.'))
        )
      )
      ->append(
        $this->kits->checkbox('allow_publish')
          ->setTitle($this->t('Allow Publication'))
          ->setDescription($this->t('Allow items with `status: 1` to be published.'))
      )
      ->append($this->kits->submit()->setValue($this->t('Add')));
    return $this->formFactory->getForm();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $data = $this->yaml->decode($form_state->getValue('data'));
    if (!is_array($data) || $this->hasStringKeys($data)) {
      $form_state->setErrorByName('data', $this->t('The submitted data must a numerically indexed array.'));
      return;
    }
    foreach ($data as $item) {
      // Define sources.
      $sources = $this->getSources($item);
      if (empty($sources)) {
        $form_state->setErrorByName('data', $this->t("Each item must have a `source` or `sources` key (e.g. `source: 'http://example.com\'`)."));
        return;
      }

      // Confirm sources are valid.
      foreach ($sources as $source) {
        if (!$this->isValidSource($source)) {
          $form_state->setErrorByName('data', $this->t('The source "@source" is not a valid source.', [
            '@source' => $item['source'],
          ]));
          return;
        }
        if ($this->getMediaIdByUrl($source)) {
          $form_state->setErrorByName('data', $this->t('A media entity for "@source" already exists.', [
            '@source' => $item['source'],
          ]));
          return;
        }
      }

      // Confirm Clips are valid, if there are any.
      if (isset($item['clips'])) {
        if (!is_array($item['clips'])) {
          $form_state->setErrorByName('data', $this->t('The "@source" `clips` must be an array of clips.', [
            '@source' => $item['source'],
          ]));
          return;
        }
        foreach ($item['clips'] as $key => $clip) {
          if (empty($clip['title']) || !is_string($clip['title'])) {
            $form_state->setErrorByName('data', $this->t("Clip @clip must have a Title (e.g. `title: 'Example'`).", [
              '@clip' => $key,
            ]));
            return;
          }
          if (!isset($clip['in']) || !$this->isValidTimestamp($clip['in'])) {
            $form_state->setErrorByName('data', $this->t('Clip "@clip" must have a valid "In Point" (e.g. `in: \'HH:MM:SS:FF\').', [
              '@clip' => $clip['title'],
            ]));
            return;
          }
          if (!isset($clip['out']) || !$this->isValidTimestamp($clip['out'])) {
            $form_state->setErrorByName('data', $this->t('Clip "@clip" must have a valid "Out Point" (e.g. `out: \'HH:MM:SS:FF\).', [
              '@clip' => $clip['title'],
            ]));
            return;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = $this->yaml->decode($form_state->getValue('data'));
    $isPublicationAllowed = (bool) $form_state->getValue('allow_publish');
    foreach ($data as $item) {
      // Create the remote video media entity & publish if status is not empty.
      $media = Media::create([
        'bundle' => 'remote_video',
        'field_media_oembed_video' => $this->getSources($item)[0],
        'status' => ($isPublicationAllowed && !empty($item['status'])) ? 1 : 0,
      ]);
      $media->save();
      $mediaId = $media->id();
      $this->messenger->addStatus($this->t('<a href="/media/@mid/edit">@name</a> created@published.', [
        '@mid' => $mediaId,
        '@name' => $media->get('name')->value,
        '@published' => $media->get('status')->value ? ' & published' : '',
      ]));

      // Create clips, if any are defined for this item.
      if (isset($item['clips'])) {
        foreach ($item['clips'] as $clip) {
          Clip::create([
            'bundle' => 'clip',
            'field_title' => $clip['title'],
            'field_media' => $mediaId,
            'field_in' => $clip['in'],
            'field_out' => $clip['out'],
            // @todo support 'field_status' => $isPublicationAllowed && !empty($clip['status']) ? 1 : 0,
          ])->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getExample() {
    $examplesPath = $this->modules->getPath('clip') . '/examples';
    $files = array_slice(scandir($examplesPath), 2);
    return file_get_contents($examplesPath . '/' . $files[rand(0, count($files) - 1)]);
  }

  /**
   * {@inheritdoc}
   */
  private function getSources(array $item): array {
    if (isset($item['source'])) {
      return [$item['source']];
    }
    elseif (isset($item['sources'])) {
      return $item['sources'];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  private function getMediaIdByUrl($url): ?int {
    $url = $this->urlWrapperFactory->getUrlWrapper($url);
    if (!$url) {
      return NULL;
    }
    $result = $this->entityTypeManager->getStorage('media')
      ->getQuery()
      ->accessCheck()
      ->condition('field_media_oembed_video', $url->getCompanyMachineName(), 'CONTAINS')
      ->condition('field_media_oembed_video', $url->getIdQueryString(), 'CONTAINS')
      ->range(0, 1)
      ->execute();
    return !empty($result);
  }

  /**
   * {@inheritdoc}
   */
  private function isValidSource($string): bool {
    if (!is_string($string)) {
      return FALSE;
    }
    return (bool) $this->urlWrapperFactory->getUrlWrapper($string)?->getId();
  }

  /**
   * {@inheritdoc}
   */
  private function isValidTimestamp($string): bool {
    if (!is_string($string)) {
      return FALSE;
    }
    return preg_match('/^\d{2}:\d{2}:\d{2}:\d{2}$/', $string) !== FALSE;
  }

  /**
   * {@inheritdoc}
   *
   * @see https://stackoverflow.com/a/4254008
   */
  public function hasStringKeys(array $array): bool {
    return count(array_filter(array_keys($array), 'is_string')) > 0;
  }

}
