# Clip
A module that allows you to create playable Clips from playable Media entities.

## What is a Clip?
A Clip is not a _copy_ of a referenced playable media entity
(e.g. "Remote Video") -- it merely _refers_ to it. A Clip is defined by
"In Point" & "Out Point" timecodes. It is up to the Clip player to queue up
the referenced playable media entity to the "In Point" & stop playing when the
"Out Point" is reached.

## Why Create Clips?
There are many reasons to create Clips. A Clip is _particularly_ useful when
you need to write an Article about a small portion of a long broadcast. Instead
of embedding the entire broadcast as a "Remote Video" in the Article (and
asking the reader to manually search for a portion of the video), you can embed
a Clip -- which will auto-queue the broadcast to the relevant section & stop
playing when appropriate. If your Article is covering a live broadcast, you can
publish your Clip & related Article _before_ the live broadcast concludes!

A Clip's "In Point" & "Out Point" are stored as "SMPTE timecodes"
(hour:minute:second:frame). A video content editor may choose to provide a YAML
list of Clips related to a video they've just rendered. Import support for
common video editing software file standards is on the roadmap.

## Usage

### Clips Import
You can quickly create a Remote Video entity & define related Clips by importing
a YAML file.

1. Visit `/media/add/remote_videos`.
2. Click the "Example" bar below the "Data" textarea.
3. Copy the example into the "Data" textarea.
4. Click the "Add" button.
5. Click on one of the new Remote Video entities in the status message once the
   new page loads.
6. Choose any Clip to the right of the video player & click the Play button.

### Manual Clip Definition
You need to create a Remote Video entity & define Clips by setting
In/Out points.

1. Visit `/media/add/remote_video`, fill the necessary fields & submit the form.
2. Click on the link for that new entity.
3. If the Remote Video supports Clip definition, the Clip Editor will appear in
   the form.
4. Start playing the video.
5. Wait a few seconds, then click the `{` button to set your first "In Point".
6. Wait a few more seconds, then click the `}` button to set your first
   "Out Point".
7. Enter a title for the Clip in the "Title" field.
8. Click the "Save" button to save the Clip.
9. Repeat the process to create more Clips.
10. Click on a circular "radio" button in the Clips table to load a Clip.
11. Click the `{` or `}` buttons to modify the Clip's In/Out points.
12. Click the "Save" button to update the Clip.

## Roadmap
This module is still in early development.

Here are some of the features that are planned:

### Providing a Clip Media Entity
The "Clip" entity should be converted to a media entity so Clips appear in the
Media Library.
For now, the "Clip" entity is a content entity. The following steps should be
taken, in order, to help protect defined Clips.

1. Convert the "Clip" entity to a media entity.
2. Make it easy to see which entities are referring to a Clip.
3. Clips that are being referred to by other entities cannot be deleted.
4. Playable media entities that have Clips defined cannot be deleted.

### Tags
Each Clip should have a "Tags" field to help ease Clip search processes.

### Providing Clip-Supporting Media Players
The source media entity is not being modified by this module. This module merely
allows the definition of Clips which refer to the source & have in/out data.
If a Clip is embedded on a page, the Media Player needs a little extra logic
to start & stop playback at the appropriate times.

### Add Support for "Vimeo" Remote Videos
This module only supports defining Clips for "YouTube" Remote Videos at this
time.

### Centralized Clip Editor
The Clip editor currently appears in the Remote Video edit form. A similar
editor should appear as an option in the main admin menu. When loaded, any
Video or Remote Video entity can be loaded into the editor. If the entity has
any Clips, they will be loaded into the editor as well.

### Automatic Clip Definition
Some remote video providers allow content creators to define Segments for
their uploaded videos.
This module should provide "Create Clips from Segments" functionality,
if possible.

### Video Editing Software Compatibility
This module should support import of a list of Clips in a format well-supported
by video editing software. A Clip is a defined segment of the main sequence
(e.g. a "chapter") -- not an _edit_ of the main sequence.

A video editor should ideally be able to define Clips as they're finishing up
the editing process within their video editing software.

* [Premiere Pro - Chapters](https://www.provideocoalition.com/from-markers-to-youtube-video-chapters-with-adobe-premiere-pro/)
* [Final Cut - Chapters](https://support.apple.com/guide/final-cut-pro/add-chapter-markers-verc9cacc031/mac)

Research is required to identify the best approach & file format to use.
AAF is a modern file format
that should be used if possible. Consider supporting EDL files if AAF is not
ideal for this use case.
The processes used to define BD/DVD chapters & YouTube chapters should be used
as primary reference.
