<?php

namespace Drupal\clip\Service;

use Drupal\clip\UrlWrapperInterface;

/**
 * Implements Interface.
 */
interface UrlWrapperFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getUrlWrapper(string $url): ?UrlWrapperInterface;

}
