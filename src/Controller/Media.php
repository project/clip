<?php

namespace Drupal\clip\Controller;

use Drupal\clip\Service\ClipFactoryInterface;
use Drupal\clip\Service\UrlWrapperFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\formfactory\Services\FormFactoryInterface;
use Drupal\formfactorykits\Services\FormFactoryKitsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Container Injection Interface.
 */
class Media extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Module Extension List.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $modules;

  /**
   * Form Factory Interface.
   *
   * @var \Drupal\formfactory\Services\FormFactoryInterface
   */
  protected FormFactoryInterface $formFactory;

  /**
   * Form Factory Kits Interface.
   *
   * @var \Drupal\formfactorykits\Services\FormFactoryKitsInterface
   */
  protected FormFactoryKitsInterface $kits;

  /**
   * Clip Factory Interface.
   *
   * @var \Drupal\clip\Service\ClipFactoryInterface
   */
  protected ClipFactoryInterface $clipFactory;

  /**
   * Url Wrapper Factory Interface.
   *
   * @var \Drupal\clip\Service\UrlWrapperFactoryInterface
   */
  protected UrlWrapperFactoryInterface $providerFactory;

  /**
   * Route Match Interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private RouteMatchInterface $routeMatch;

  /**
   * Yaml.
   *
   * @var \Drupal\Component\Serialization\Yaml
   */
  private Yaml $yaml;

  /**
   * Kill Switch.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  private KillSwitch $killSwitch;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RouteMatchInterface $route_match,
    Yaml $yaml,
    KillSwitch $kill_switch,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
    $this->yaml = $yaml;
    $this->killSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('serialization.yaml'),
      $container->get('page_cache_kill_switch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function export() {
    $data = [];
    $this->killSwitch->trigger();
    $media = $this->entityTypeManager->getStorage('media')
      ->load($this->getMediaId());
    $entity['source'] = $media->get('field_media_oembed_video')->value;
    $clipIds = $this->entityTypeManager->getStorage('clip')
      ->getQuery()
      ->accessCheck()
      ->condition('field_media', $media->id())
      ->sort('field_in')
      ->execute();
    foreach ($clipIds as $clipId) {
      $clip = $this->entityTypeManager->getStorage('clip')->load($clipId);
      $entity['clips'][] = [
        'title' => $clip->get('field_title')->value,
        'in' => $clip->get('field_in')->value,
        'out' => $clip->get('field_out')->value,
      ];
    }
    $data[] = $entity;

    return [
      '#type' => 'inline_template',
      '#template' => '<pre>{{ yaml }}</pre>',
      '#context' => [
        'yaml' => $this->yaml->encode($data),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function getMediaId(): int {
    return $this->routeMatch->getRawParameter('media');
  }

}
