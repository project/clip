<?php

namespace Drupal\clip;

/**
 * Implements Url Wrapper.
 */
abstract class UrlWrapper implements UrlWrapperInterface {

  /**
   * URL.
   *
   * @var string
   */
  protected string $url;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $url) {
    $this->url = $url;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderDomain(): string {
    return $this->getCompanyMachineName() . '.com';
  }

}
