<?php

namespace Drupal\clip\Service;

use Drupal\clip\UrlWrapperInterface;
use Drupal\clip\YouTubeUrlWrapper;

/**
 * Implements Url Wrapper Factory.
 */
class UrlWrapperFactory implements UrlWrapperFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getUrlWrapper(string $url): ?UrlWrapperInterface {
    if (!$this->isValidUrl($url)) {
      return NULL;
    }
    $host = parse_url($url, PHP_URL_HOST);
    if (!str_starts_with($host, 'www.')) {
      $host = 'www.' . $host;
    }
    if ($host === 'www.youtube.com') {
      return new YouTubeUrlWrapper($url);
    }
    // @todo support more providers
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  private function isValidUrl(string $url) {
    // @todo implement method
    return TRUE;
  }

}
