let youTubeIframePlayer;
let tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
function onYouTubeIframeAPIReady() {
  youTubeIframePlayer = new YT.Player('player', {
    videoId: Drupal.behaviors.clipMediaEntityEditForm.getVideoSourceUrl().searchParams.get('v'),
    playerVars: {
      'playsinline': 1,
      'rel': 0,
      'disablekb': 1,
    },
    events: {
      'onReady': function (event) {
        Drupal.behaviors.clipMediaEntityEditForm.onPlayerAttach();
      },
      'onStateChange': function (event) {
        if (event.data == YT.PlayerState.PLAYING) {
          Drupal.behaviors.clipMediaEntityEditForm.onPlayerPlay();
        }
        else {
          Drupal.behaviors.clipMediaEntityEditForm.onPlayerPause();
        }
      }
    }
  });
}

class ClipPlayer extends AbstractClipPlayer {
  pause() {
    youTubeIframePlayer.pauseVideo();
  }

  play() {
    youTubeIframePlayer.playVideo();
  }

  setPlaybackRate(rate) {
    youTubeIframePlayer.setPlaybackRate(rate);
  }

  isPlaying() {
    return youTubeIframePlayer.getPlayerState() === YT.PlayerState.PLAYING;
  }

  getCurrentTime() {
    return youTubeIframePlayer.getCurrentTime();
  }

  seekToSecond(second) {
    youTubeIframePlayer.seekTo(second);
  }

  seekToTimecode(timecode) {
    youTubeIframePlayer.seekTo(this.getTime(timecode));
  }

  getVolume() {
    return youTubeIframePlayer.getVolume() / 100;
  }

  setVolume(volume) {
    youTubeIframePlayer.setVolume(volume * 100);
  }
}
